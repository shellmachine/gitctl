# gitctl

A) Client Setup

1) Create a script in .local/bin/gitctl with the following content:
```
#!/bin/sh
ssh -i $HOME/.ssh/id_ed25519_gitctl gitctl "$@"
```

2) Create a special gitctl SSH key pair:
```
ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519_gitctl
```

3) add ~/.ssh/config entry
```
host gitctl
user git
hostname xxxxxxxxx
identityfile ~/.ssh/id_ed25519_gitctl
identitiesonly yes
```


B) Server Setup
1) Add a user called "git" to the system
2) Add ED25519 public key from client to authorized_keys on server with command=:
```
command="/usr/home/git/.gitctl/gitctl $SSH_ORIGINAL_COMMAND" ssh-ed25519 xxxxxxxxxxxxxxxxxxxxxxx
```
3) Deploy gitcl script on server as e.g. /usr/home/git/.gitctl/gitctl and make it executable
